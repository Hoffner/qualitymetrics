import sys
import argparse

def main():

    parser = argparse.ArgumentParser(description='Calculate and display the quality metrics of a C ++ class')
    parser.add_argument('stringPath', type=str, help='The string path of the C++ class')
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    args = parser.parse_args()

    linesNumber = 0
    with open(args.stringPath, "r") as mon_fichier:
        for ligne in mon_fichier:
            linesNumber += 1

    if args.verbose:
        print("number of line is :", linesNumber)
    else:
        print(linesNumber)

if __name__ == "__main__":
    main()
